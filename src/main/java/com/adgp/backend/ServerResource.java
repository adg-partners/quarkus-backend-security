package com.adgp.backend;

import io.quarkus.runtime.Quarkus;
import io.quarkus.security.Authenticated;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.InternalServerErrorException;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.SecurityContext;
import org.eclipse.microprofile.jwt.JsonWebToken;

@Path("/hello")
@Authenticated
public class ServerResource {

    @Inject
    JsonWebToken jwt;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello(@Context SecurityContext ctx) {
        String name;
        if (ctx.getUserPrincipal() == null) {
            name = "anonymous";
        } else if (!ctx.getUserPrincipal().getName().equals(jwt.getName())) {
            throw new InternalServerErrorException("Principal and JsonWebToken names do not match");
        } else {
            name = ctx.getUserPrincipal().getName();
        }

        System.out.println(String.format("hello + %s,"
                        + " isHttps: %s,"
                        + " authScheme: %s,"
                        + " hasJWT: %s,"
                        + " [scp]: %s,"
                        + " [tid]: %s,"
                        + " [sub]: %s",
                name, ctx.isSecure(), ctx.getAuthenticationScheme(), hasJwt(), jwt.getClaim("scp"), jwt.getClaim("tid"), jwt.getClaim("sub")));
        return "Hello from RESTEasy Reactive";
    }

    private boolean hasJwt() {
        return jwt.getClaimNames() != null;
    }

    public static void main(String[] args) {
        System.out.println("Running main method");
        Quarkus.run(args);
    }
}
