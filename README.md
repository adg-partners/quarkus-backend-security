# Tutorial Step 3

Run your backend: 
```shell script
./mvnw compile quarkus:dev
```
Récupérer un token valide

```shell script
curl --location --verbose 'localhost:8080/hello' -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ii1LSTNROW5OUjdiUm9meG1lWm9YcWJIWkdldyJ9.eyJhdWQiOiJlZDg4N2YyMi1kMzQxLTQ3N2UtODM0OC03MTcyNTU1YTFiODIiLCJpc3MiOiJodHRwczovL2xvZ2luLm1pY3Jvc29mdG9ubGluZS5jb20vN2QwYzQyM2MtNWFkNC00Zjk4LTllNDQtMjgwNDZiODNiMTA3L3YyLjAiLCJpYXQiOjE2OTE1MjgxNTAsIm5iZiI6MTY5MTUyODE1MCwiZXhwIjoxNjkxNTMyOTA4LCJhaW8iOiJBWFFBaS84VUFBQUFHeGdLd2hhaHZIQ0VYMGJIUlVUaWJmdG1wTFlTY3p5MFJuMlBhM2ZmV3A1U3BlWDBLUExQbkg1VS9NQjBaTmZwQkx5ajhqOHNFbFE4RXRKUktsRTdHdThKTW9HczJtODRCNkpIRm1JQU5ETmhJUUpycGZjS3JvQ29OY09SSHo3L0libk1XOVQ4TmhTdjY1dkROcS92VEE9PSIsImF6cCI6Ijg4OTUxZjM4LTZhZWEtNGQ2MS1hMjUzLTUzMmEwOTgyNDQ0ZiIsImF6cGFjciI6IjAiLCJuYW1lIjoiQXJuYXVkIiwib2lkIjoiYzI4NDViN2QtNzJhNS00OGIyLWE3OTAtZDg4MjA5YTdiZTg1IiwicHJlZmVycmVkX3VzZXJuYW1lIjoiYWRtaW4tYWRlQE0zNjV4MDM0ODE2NTUub25taWNyb3NvZnQuY29tIiwicmgiOiIwLkFYMEFQRUlNZmRSYW1FLWVSQ2dFYTRPeEJ5Sl9pTzFCMDM1SGcwaHhjbFZhRzRLYUFPNC4iLCJzY3AiOiJxdWFya3VzLWJhY2tlbmQuYWNjZXNzIiwic3ViIjoiRjMtWVdqYTNiN3hWV29mZF8wSUpOZFZjLWRscUhoSDBkTkg4RExISUN0RSIsInRpZCI6IjdkMGM0MjNjLTVhZDQtNGY5OC05ZTQ0LTI4MDQ2YjgzYjEwNyIsInV0aSI6Ik9Lc1Q0ZXZuT1VxZGpuS1RDbzBsQUEiLCJ2ZXIiOiIyLjAifQ.iQCiXqMZhvhhfyxH0wJ28EwHDtihbjjUog1gDWzm0LQkL8oAnPkZ21y9v-Mw240NUC6ncfwyNgk5fkAy7bDDxDOBtL_MveoWlAylJr7HUPWC2CvtUhsOrWCX9yWvQ8uZXFGVMxlhgXMRWyrDPcu-5_4kNa76VxX_fb-Koy3xONmsqlirWX_ujyGdgutznlB3cSxCztJt6KmW4YwxHGqXPHmz7rM_5p7dk1Wn32W5QD2lOTWxHtszKkhiAIrWAYB0OVvFqevNDv3fsZegULWpmUIOK2L5RbyE8fjihKnt_L1ijI3gdTG8RFJ3W4u_dX5YcONxKN_UjPhPnlHwcKcKzA"
```

You will see:
```shell script
*   Trying 127.0.0.1:8080...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /hello HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.81.0
> Accept: */*
> Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ii1LSTNROW5OUjdiUm9meG1lWm9YcWJIWkdldyJ9.eyJhdWQiOiJlZDg4N2YyMi1kMzQxLTQ3N2UtODM0OC03MTcyNTU1YTFiODIiLCJpc3MiOiJodHRwczovL2xvZ2luLm1pY3Jvc29mdG9ubGluZS5jb20vN2QwYzQyM2MtNWFkNC00Zjk4LTllNDQtMjgwNDZiODNiMTA3L3YyLjAiLCJpYXQiOjE2OTE1MjgxNTAsIm5iZiI6MTY5MTUyODE1MCwiZXhwIjoxNjkxNTMyOTA4LCJhaW8iOiJBWFFBaS84VUFBQUFHeGdLd2hhaHZIQ0VYMGJIUlVUaWJmdG1wTFlTY3p5MFJuMlBhM2ZmV3A1U3BlWDBLUExQbkg1VS9NQjBaTmZwQkx5ajhqOHNFbFE4RXRKUktsRTdHdThKTW9HczJtODRCNkpIRm1JQU5ETmhJUUpycGZjS3JvQ29OY09SSHo3L0libk1XOVQ4TmhTdjY1dkROcS92VEE9PSIsImF6cCI6Ijg4OTUxZjM4LTZhZWEtNGQ2MS1hMjUzLTUzMmEwOTgyNDQ0ZiIsImF6cGFjciI6IjAiLCJuYW1lIjoiQXJuYXVkIiwib2lkIjoiYzI4NDViN2QtNzJhNS00OGIyLWE3OTAtZDg4MjA5YTdiZTg1IiwicHJlZmVycmVkX3VzZXJuYW1lIjoiYWRtaW4tYWRlQE0zNjV4MDM0ODE2NTUub25taWNyb3NvZnQuY29tIiwicmgiOiIwLkFYMEFQRUlNZmRSYW1FLWVSQ2dFYTRPeEJ5Sl9pTzFCMDM1SGcwaHhjbFZhRzRLYUFPNC4iLCJzY3AiOiJxdWFya3VzLWJhY2tlbmQuYWNjZXNzIiwic3ViIjoiRjMtWVdqYTNiN3hWV29mZF8wSUpOZFZjLWRscUhoSDBkTkg4RExISUN0RSIsInRpZCI6IjdkMGM0MjNjLTVhZDQtNGY5OC05ZTQ0LTI4MDQ2YjgzYjEwNyIsInV0aSI6Ik9Lc1Q0ZXZuT1VxZGpuS1RDbzBsQUEiLCJ2ZXIiOiIyLjAifQ.iQCiXqMZhvhhfyxH0wJ28EwHDtihbjjUog1gDWzm0LQkL8oAnPkZ21y9v-Mw240NUC6ncfwyNgk5fkAy7bDDxDOBtL_MveoWlAylJr7HUPWC2CvtUhsOrWCX9yWvQ8uZXFGVMxlhgXMRWyrDPcu-5_4kNa76VxX_fb-Koy3xONmsqlirWX_ujyGdgutznlB3cSxCztJt6KmW4YwxHGqXPHmz7rM_5p7dk1Wn32W5QD2lOTWxHtszKkhiAIrWAYB0OVvFqevNDv3fsZegULWpmUIOK2L5RbyE8fjihKnt_L1ijI3gdTG8RFJ3W4u_dX5YcONxKN_UjPhPnlHwcKcKzA
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< content-length: 28
< Content-Type: text/plain;charset=UTF-8
< 
* Connection #0 to host localhost left intact
Hello from RESTEasy Reactive
```


# Generic information: code-with-quarkus

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application, packaged as an _über-jar_, is now runnable using `java -jar target/*-runner.jar`.

## Creating a native executable

You can create a native executable using: 
```shell script
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/code-with-quarkus-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.

## Related Guides

- RESTEasy Reactive ([guide](https://quarkus.io/guides/resteasy-reactive)): A Jakarta REST implementation utilizing build time processing and Vert.x. This extension is not compatible with the quarkus-resteasy extension, or any of the extensions that depend on it.
- SmallRye JWT ([guide](https://quarkus.io/guides/security-jwt)): Secure your applications with JSON Web Token

## Provided Code

### RESTEasy Reactive

Easily start your Reactive RESTful Web Services

[Related guide section...](https://quarkus.io/guides/getting-started-reactive#reactive-jax-rs-resources)
